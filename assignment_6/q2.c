//Recursive function to print Fibonacci sequence.
#include <stdio.h>

int fibonacciSeq(int n);

int main()
{
    int num, i;
    printf("Enter the total number of terms you need: ");
    scanf("%d", &num);

//iteration to get the sequence.
    for(i = 0; i <= num; i++)
    {
        printf("%d\n", fibonacciSeq(i));
    }

    return 0;
}

//recursive function that gives the respective Fibonacci number.
int fibonacciSeq(int n)
{
    if(n == 0 || n == 1)
    {
        return n;
    }
    else
    {
        return fibonacciSeq(n-1) + fibonacciSeq(n-2);
    }
}

