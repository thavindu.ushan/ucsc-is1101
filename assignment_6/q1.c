//program to prints the number triangle.
#include <stdio.h>


void pattern(int n);
int pattern2(int n);

int main()
{
    int num;

    printf("Enter the number of rows: ");
    scanf("%d", &num);

    pattern(num);

    return 0;
}

//This prints the number of rows.
void pattern(int n)
{
    if(n > 1)
    {
        pattern(n-1);
    }
    printf("%d\n", pattern2(n));
}

//This completes the triangle.
int pattern2(int n)
{
    if(n > 1)
    {
        printf("%d", n);
        pattern2(n - 1);
    }
}
