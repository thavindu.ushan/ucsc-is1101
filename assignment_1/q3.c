#include <stdio.h>
//This program is for swap two integers.

int main() {
    int num1, num2, x;
    printf("Enter number 1: ");
    scanf("%d", &num1);
    printf("Enter number 2: ");
    scanf("%d", &num2);
    x = num1;
    num1 = num2;
    num2 = x;
    printf("Now this is the number 1: %d and this is the number 2: %d ", num1, num2);
    return 0;
}
