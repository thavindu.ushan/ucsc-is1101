#include <stdio.h>
//This program is for find the multiplication of two floating-type numbers.

int main() {
    float num1,num2,answer;
    printf("Enter your first floating-type number: ");
    scanf("%f", &num1);
    printf("Enter your second floating-type number: ");
    scanf("%f", &num2);
    answer = num1 * num2;
    printf("This is your answer: %f", answer);
    return 0;
}
