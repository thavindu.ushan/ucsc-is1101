#include <stdio.h>
//This program is for find the area of a disk.

int main()
{
   float r,area;
   printf("Enter the radius: ");
   scanf("%f", &r);
   const float pi = 3.142;
   area = pi * (r*r);
   printf("This is the area of the disk: %f", area);
   return 0;
}
