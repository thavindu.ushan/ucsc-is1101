#include <stdio.h>

int main()
{
    int num, total;

    printf("Enter a number: ");
    scanf("%d", &num);

    while (num > 0)
    {
        total += num;
        printf("Enter a number: ");
        scanf("%d", &num);
    }

    printf("Total value   : %d\n", total);

    return 0;
}
