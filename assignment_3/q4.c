#include <stdio.h>

int main()
{
    int num, c;
    printf("Enter a number: ");
    scanf("%d", &num);

    for (c =1; c <= num; c++)
    {
        if(num %c == 0)
        {
            printf("factor of %d : %d\n", num, c);
        }
    }

    return 0;
}
