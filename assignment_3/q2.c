#include<stdio.h>

int main()
{
  int num, c, fac = 0;

  printf("Enter a number: ");
  scanf("%d", &num);

  for(c = 1;c <= num; c++)
  {
      if(num % c == 0)
      {
          fac++;
      }
  }
  if(num == 0 || num == 1)
  {
      printf("%d is neither a composite nor a prime number.\n", num);
  }
  else if(fac == 2)
  {
      printf("%d is a prime number.\n", num);
  }
  else
  {
      printf("%d is a composite number.\n", num);
  }

  return 0;

}
