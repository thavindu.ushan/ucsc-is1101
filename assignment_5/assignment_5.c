//functions
#include <stdio.h>

int people(int p1);
int income(int p1, int n1);
int spendings(int n1);
int outcome(int p2, int p3);

int main(){
    int tktPrice, attendees, revenue, cost, profit, maxprofit, maxtktPrice;

    for(int tktPrice = 5; tktPrice <= 35; tktPrice++){

        attendees = people(tktPrice);
        revenue = income(tktPrice, attendees);
        cost = spendings(attendees);
        profit = outcome(revenue, cost);
        printf("Ticket Price: %d   Profit: %d\n", tktPrice, profit);

        if (profit > maxprofit){
            maxprofit = profit; //finding the highest profit.
            maxtktPrice = tktPrice; //finding the highest profitable ticket price.
        }

        tktPrice += 4; // checking the condition for ticket prices of 5,10,15,...
    }
    printf("\n");
    printf("Highest Profit: %d   Highest profitable ticket Price: %d\n", maxprofit, maxtktPrice);
    return 0;
}

int people(int p1){
    int x = p1 - 15; // finding the difference between the current ticket Price and the 15.
    if(x == 0){
        return 120; //if x = 0, that means current ticket price = 120; Therefore attendees count will be 120.
    }
    else{
        return (120 - x * 4);
        /*changing the ticket price by 5 rupees will result increasing or decreasing attendees count by 20.
        Therefore changing ticket price by 1 rupee will result changing the attendees count by 4*/
    }
}

int income(int p1, int n1){
    return p1 * n1;
}

int spendings(int n1){
    return 500 + 3 * n1;
}

int outcome(int p2, int p3){
    return p2 - p3;
}
