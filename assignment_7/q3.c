//program to add and multiply two matrices.
#include <stdio.h>

int main()
{
    int i, j, k;

    //taking 1st matrix as a input.
    int row1, col1;
    printf("Enter the number of rows in matrix 1: ");
    scanf("%d", &row1);
    printf("Enter the number of columns in matrix 1: ");
    scanf("%d", &col1);

    printf("\n");
    int matrix1[row1][col1];

    for(i = 0; i < row1; i++)
    {
        for(j = 0; j < col1; j++)
        {
            printf("Enter the [%d][%d] element in matrix 1: ", i, j);
            scanf("%d", &matrix1[i][j]);
        }
    }

    printf("\n");

    //taking 2nd matrix as a input.
    int row2, col2;
    printf("Enter the number of rows in matrix 2: ");
    scanf("%d", &row2);
    printf("Enter the number of columns in matrix 2: ");
    scanf("%d", &col2);

    printf("\n");
    int matrix2[row2][col2];
    int matrixA[row1][col1];
    int matrixP[row1][col2];
    int total = 0;

    for(i = 0; i < row2; i++)
    {
        for(j = 0; j < col2; j++)
        {
            printf("Enter the [%d][%d] element in matrix 2: ", i, j);
            scanf("%d", &matrix2[i][j]);
        }
    }

    printf("\n");

    if(row1 != row2 || col1 != col2)//to add two matrices, number of rows and columns in each matrix must be equal.
    {
        printf("Addition of the two matrices: \n\n");
        printf("Cant add..., number of rows and number of columns in each matrix must be equal.\n\n");
    }
    else
    {
        //adding two matrices.
        for(i = 0; i < row1; i++)
            {
                for(j =0; j < col1; j++)
                {
                    matrixA[i][j] = matrix1[i][j] + matrix2[i][j];
                }
            }

            printf("Addition of the two matrices: \n\n");

            //printing the result.
            for(i = 0; i < row1; i++)
            {
                for(j =0; j < col1; j++)
                {
                    printf("%d ", matrixA[i][j]);
                }
                printf("\n");
            }
    }

    printf("\n");

    if(col1 != row2) //to multiply two matrices, number of columns in the 1st matrix must be equal to number of rows in the second matrix.
    {
        printf("Multiplication of the two matrices: \n\n");
        printf("cant multiply..., number of columns in the 1st matrix must be equal to number of rows in the second matrix.\n\n");
    }
    else
    {
        //multiplying the tow matrices.
         for(i = 0; i < row1; i++)
        {
            for(j = 0; j < col2; j++)
            {
                for(k = 0; k < col1; k++)
                {
                    total += matrix1[i][k] * matrix2[k][j];
                }
                matrixP[i][j] = total;
                total = 0;
            }
        }

        printf("Multiplication of the two matrices: \n\n");

        //printing the result.
        for(i = 0; i < row1; i++)
        {
            for(j = 0; j < col2; j++)
            {
                printf("%d ", matrixP[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}
