//program to find the frequency of a character in a given string using command-line arguments.
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
    int i, j, len, freq= 0;
    char c;

    if(argc == 1)//if user does not give any command-line arguments, this message will show up.
    {
        printf("You should give input as command-line arguments.\n");
    }
    else
    {
        len = strlen(argv[1]);

        if (len != 1) // Second argument must be a character.
        {
            printf("You should input arguments as <character> <string1> <string2>...\n");
        }
        else
        {
            c = *argv[1];

            printf("Entered letter: %c\n", c);

            printf("Entered string: ");

            for(i = 2; i <= argc-1; i++) //to get the strings which are after the second argument.
            {
                len = 0;
                len = strlen(argv[i]);
                for(j = 0; j < len; j++) //to search all the elements in those arguments
                {
                    if(c == argv[i][j]) //if a element matches with the entered character frequency count will increase.
                    {
                        freq++;
                    }
                    printf("%c", argv[i][j]); //prints the entered string.
                }
                printf(" "); //prints spaces between strings.
            }

            printf("\n\nFrequency of the %c in the entered string: %d\n", c, freq); //prints the result.
        }
    }

    return 0;
}
