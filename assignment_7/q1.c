//program to reverse a string.
#include <stdio.h>
#include <string.h>

int main()
{
    char string[100];
    int i, len;

    printf("Enter a string  : ");
    gets(string);

    len = strlen(string);

    printf("Reversed string : ");

    for(i = len-1; i >= 0; i--)
    {
        printf("%c", string[i]);
    }

    printf("\n");

    return 0;
}
