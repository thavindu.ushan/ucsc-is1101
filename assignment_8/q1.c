//program to read and print student details using struct.
#include <stdio.h>
#include <string.h>

struct student //struct definition
{
    char stName[50];
    char stSub[20];
    int stMarks;

};

void printStudent(struct student students);

int main()
{
    int i, j, st;
    printf("Enter the number of students: ");
    scanf("%d", &st);
    printf("\n");

    struct student students[st];

    if(st < 5)
    {
        printf("Student number must be greater than 5.\n"); //students are must be greater than 5.
    }
    else
    {
        for(i = 0; i < st; i++) //taking student details.
        {
            printf("Enter the Student's First name : ");
            scanf("%s", students[i].stName);
            printf("Enter the Subject              : ");
            scanf("%s", students[i].stSub);
            printf("Enter the marks                : ");
            scanf("%d", &students[i].stMarks);
            printf("\n");

        }

        printf("---------------------------------------------------------------\n");
        printf("Student details: \n\n");

        for(j = 0; j < st; j++) //printing student details.
        {
            printf("Student: %d\n", j+1);
            printStudent(students[j]);
        }
    }

    return 0;
}

void printStudent(struct student students) //function to print student details.
{
    printf("Student Name: %s\n", students.stName);
    printf("Student Subject: %s\n", students.stSub);
    printf("Student Marks: %d\n", students.stMarks);
    printf("\n");
}
